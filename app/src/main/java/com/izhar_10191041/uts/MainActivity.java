package com.izhar_10191041.uts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;

    GridLayoutManager gridLayoutManager;

    DashBoardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rlmenu);

        addData();
        gridLayoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashBoardAdapter(datamenu);
        recyclerView.setAdapter(adapter);
    }

    public void addData(){
        datamenu = new ArrayList<>();
        datamenu.add(new SetterGetter("Tiket Hotel", "logomenu1"));
        datamenu.add(new SetterGetter("Villa", "logomenu2"));
        datamenu.add(new SetterGetter("Tiket Pesawat", "logomenu3"));
        datamenu.add(new SetterGetter("Tiket Kereta", "logomenu4"));
        datamenu.add(new SetterGetter("Game", "logomenu5"));
        datamenu.add(new SetterGetter("Tiket Bus", "logomenu6"));
        datamenu.add(new SetterGetter("Financial", "logomenu7"));
        datamenu.add(new SetterGetter("Asuransi", "logomenu8"));
        datamenu.add(new SetterGetter("TravelPay", "logomenu9"));
    }
}